<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class InvalidRequesterInfoException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class InvalidRequesterInfoException extends ViesVerifierException
{}
