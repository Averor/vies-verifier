<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class GlobalMaxConcurrentReqTimeException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class GlobalMaxConcurrentReqTimeException extends ViesVerifierException
{}
