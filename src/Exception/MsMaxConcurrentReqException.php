<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class MsMaxConcurrentReqException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class MsMaxConcurrentReqException extends ViesVerifierException
{}
