<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class MemberStateUnavailableException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class MemberStateUnavailableException extends ViesVerifierException
{}
