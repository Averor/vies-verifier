<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class MsMaxConcurrentReqTimeException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class MsMaxConcurrentReqTimeException extends ViesVerifierException
{}
