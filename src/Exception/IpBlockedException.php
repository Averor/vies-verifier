<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class IpBlockedException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class IpBlockedException extends ViesVerifierException
{}
