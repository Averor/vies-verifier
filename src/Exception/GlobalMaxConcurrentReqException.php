<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class GlobalMaxConcurrentReqException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class GlobalMaxConcurrentReqException extends ViesVerifierException
{}
