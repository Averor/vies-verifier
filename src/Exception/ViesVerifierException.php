<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class ViesVerifierException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class ViesVerifierException extends \RuntimeException
{}
