<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class InvalidInputException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class InvalidInputException extends ViesVerifierException
{}
