<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class ServiceUnavailableException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class ServiceUnavailableException extends ViesVerifierException
{}
