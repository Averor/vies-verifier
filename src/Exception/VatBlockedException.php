<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class VatBlockedException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class VatBlockedException extends ViesVerifierException
{}
