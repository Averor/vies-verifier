<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class ServerBusyException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class ServerBusyException extends ViesVerifierException
{}
