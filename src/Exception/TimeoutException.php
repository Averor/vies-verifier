<?php

namespace Averor\ViesVerifier\Exception;

/**
 * Class TimeoutException
 *
 * @package Averor\ViesVerifier\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class TimeoutException extends ViesVerifierException
{}
