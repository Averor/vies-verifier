<?php

namespace Averor\ViesVerifier;

use Psr\Log\NullLogger;
use SoapClient;
use SoapFault;
use stdClass;
use Psr\Log\LoggerInterface;
use Averor\ViesVerifier\Exception as Exception;

/**
 * Class ViesVerifier
 *
 * @package Averor\ViesVerifier
 * @author Averor <averor.dev@gmail.com>
 */
class ViesVerifier
{
    /** @var string VIES WSDL */
    const WSDL = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

    /** @var SoapClient */
    protected $client;

    /** @var LoggerInterface|null */
    protected $logger;

    /**
     * @param LoggerInterface|null $logger All communication with VIES can be logged using logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        $this->client = new SoapClient(
            self::WSDL,
            [
                'soap_version' => SOAP_1_1,
                'encoding' => 'UTF-8',
                'cache_wsdl' => false,
                'features' => SOAP_WAIT_ONE_WAY_CALLS,
                'trace' => 1
            ]
        );

        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * @param  string $countryId [A-Z]{2}
     * @param  string $vatNumber [0-9A-Za-z\+\*\.]{2,12}
     * @return bool
     * @throws Exception\InvalidInputException
     * @throws Exception\MemberStateUnavailableException
     * @throws Exception\ServerBusyException
     * @throws Exception\ServiceUnavailableException
     * @throws Exception\TimeoutException
     * @throws Exception\ViesVerifierException
     */
    public function verify($countryId, $vatNumber)
    {
        $countryId = strtoupper($countryId);
        $vatNumber = str_replace([' ', '-'], '', $vatNumber);

        try {

            /** @var stdClass $viesResponse */
            $viesResponse = $this->client->checkVat([
                    'countryCode' => $countryId,
                    'vatNumber' => $vatNumber
                ]
            );

            $response = (1 === (int)$viesResponse->valid);

            $this->logger->info(
                sprintf(
                    "[ViesVerifier] [countryId: %s] [vatNumber: %s] [Response: %s]",
                    $countryId,
                    $vatNumber,
                    json_encode($viesResponse)
                )
            );

            return $response;

        } catch (SoapFault $f) {

            $this->logger->error(
                sprintf(
                    "[ViesVerifier] [countryId: %s] [vatNumber: %s] [Exception: %s]",
                    $countryId,
                    $vatNumber,
                    $msg = $f->getMessage()
                ),
                ['exception' => $f]
            );

            switch ($msg) {

                // The provided CountryCode is invalid or the VAT number is empty;
                case 'INVALID_INPUT':
                    throw new Exception\InvalidInputException();

                // ???
                case 'INVALID_REQUESTER_INFO':
                    throw new Exception\InvalidRequesterInfoException();

                // The Member State service is unavailable, try again later or with another Member State;
                case 'MS_UNAVAILABLE':
                    throw new Exception\MemberStateUnavailableException();

                // The Member State service could not be reach in time, try again later or with another Member State;
                case 'TIMEOUT':
                    throw new Exception\TimeoutException();

                // ???
                case 'VAT_BLOCKED':
                    throw new Exception\VatBlockedException();

                // ???
                case 'IP_BLOCKED':
                    throw new Exception\IpBlockedException();

                // ???
                case 'GLOBAL_MAX_CONCURRENT_REQ':
                    throw new Exception\GlobalMaxConcurrentReqException();

                // ???
                case 'GLOBAL_MAX_CONCURRENT_REQ_TIME':
                    throw new Exception\GlobalMaxConcurrentReqTimeException();

                // ???
                case 'MS_MAX_CONCURRENT_REQ':
                    throw new Exception\MsMaxConcurrentReqException();

                // ???
                case 'MS_MAX_CONCURRENT_REQ_TIME':
                    throw new Exception\MsMaxConcurrentReqTimeException();

                // The service can't process your request. Try again later.
                // This error does not exist anymore (?)
                case 'SERVER_BUSY':
                    throw new Exception\ServerBusyException();

                // The SOAP service is unavailable, try again later;
                case 'SERVICE_UNAVAILABLE':
                    throw new Exception\ServiceUnavailableException();

                default:
                    throw new Exception\ViesVerifierException($msg);
            }
        }
    }
}
