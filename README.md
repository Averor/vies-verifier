# ViesVerifier
Tiny library simplifying the use of VIES VAT-UE verification service

## Usage:

###### *Instantiate*
```php
\Averor\ViesVerifier\ViesVerifier $verifier = \Averor\ViesVerifier\ViesVerifier(LoggerInterface $logger = null);
```

###### *Verify*
```php
bool $verifier->verify(string $countryId, string $vatNumber);
```